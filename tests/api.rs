use dndrs::{
    ability_scores::AbilityScore, alignments::Alignment, backgrounds::Background, classes::Class,
    conditions::Condition, damage_types::DamageType, equipment::Equipment,
    equipment_categories::EquipmentCategory, feats::Feat, features::Feature, languages::Language,
    levels::Level, magic_items::MagicItem, magic_schools::MagicSchool, monsters::Monster,
    proficiencies::Proficiency, races::Race, rule_sections::RuleSection, rules::Rule,
    skills::Skill, spells::Spell, subclasses::Subclass, subraces::Subrace, traits::Trait,
    weapon_properties::WeaponProperty,
};

#[tokio::test]
async fn test_get()
{
    let cha = dndrs::APIClient::get::<AbilityScore>("cha".into())
        .await
        .unwrap();
    assert_eq!(cha.index, "cha");

    let neutral = dndrs::APIClient::get::<Alignment>("neutral".into())
        .await
        .unwrap();
    assert_eq!(neutral.index, "neutral");

    let acolyte = dndrs::APIClient::get::<Background>("acolyte".into())
        .await
        .unwrap();
    assert_eq!(acolyte.index, "acolyte");

    let wizard = dndrs::APIClient::get::<Class>("wizard".into())
        .await
        .unwrap();
    assert_eq!(wizard.index, "wizard");

    let blinded = dndrs::APIClient::get::<Condition>("blinded".into())
        .await
        .unwrap();
    assert_eq!(blinded.index, "blinded");

    let poison = dndrs::APIClient::get::<DamageType>("poison".into())
        .await
        .unwrap();
    assert_eq!(poison.index, "poison");

    let armor = dndrs::APIClient::get::<EquipmentCategory>("armor".into())
        .await
        .unwrap();
    assert_eq!(armor.index, "armor");

    let club = dndrs::APIClient::get::<Equipment>("club".into())
        .await
        .unwrap();
    assert_eq!(club.index, "club");

    let grappler = dndrs::APIClient::get::<Feat>("grappler".into())
        .await
        .unwrap();
    assert_eq!(grappler.index, "grappler");

    let arcane_recovery = dndrs::APIClient::get::<Feature>("arcane-recovery".into())
        .await
        .unwrap();
    assert_eq!(arcane_recovery.index, "arcane-recovery");

    let common = dndrs::APIClient::get::<Language>("common".into())
        .await
        .unwrap();
    assert_eq!(common.index, "common");

    // TODO: Level type

    let adamantine_armor = dndrs::APIClient::get::<MagicItem>("adamantine-armor".into())
        .await
        .unwrap();
    assert_eq!(adamantine_armor.index, "adamantine-armor");

    let abjuration = dndrs::APIClient::get::<MagicSchool>("abjuration".into())
        .await
        .unwrap();
    assert_eq!(abjuration.index, "abjuration");

    let aboleth = dndrs::APIClient::get::<Monster>("aboleth".into())
        .await
        .unwrap();
    assert_eq!(aboleth.index, "aboleth");

    let all_armor = dndrs::APIClient::get::<Proficiency>("all-armor".into())
        .await
        .unwrap();
    assert_eq!(all_armor.index, "all-armor");

    let half_elf = dndrs::APIClient::get::<Race>("half-elf".into())
        .await
        .unwrap();
    assert_eq!(half_elf.index, "half-elf");

    let ability_checks = dndrs::APIClient::get::<RuleSection>("ability-checks".into())
        .await
        .unwrap();
    assert_eq!(ability_checks.index, "ability-checks");

    let adventuring = dndrs::APIClient::get::<Rule>("adventuring".into())
        .await
        .unwrap();
    assert_eq!(adventuring.index, "adventuring");

    let acrobatics = dndrs::APIClient::get::<Skill>("acrobatics".into())
        .await
        .unwrap();
    assert_eq!(acrobatics.index, "acrobatics");

    let magic_missile = dndrs::APIClient::get::<Spell>("magic-missile".into())
        .await
        .unwrap();
    assert_eq!(magic_missile.index, "magic-missile");

    let berserker = dndrs::APIClient::get::<Subclass>("berserker".into())
        .await
        .unwrap();
    assert_eq!(berserker.index, "berserker");

    let high_elf = dndrs::APIClient::get::<Subrace>("high-elf".into())
        .await
        .unwrap();
    assert_eq!(high_elf.index, "high-elf");

    let brave = dndrs::APIClient::get::<Trait>("brave".into())
        .await
        .unwrap();
    assert_eq!(brave.index, "brave");

    let heavy = dndrs::APIClient::get::<WeaponProperty>("heavy".into())
        .await
        .unwrap();
    assert_eq!(heavy.index, "heavy");
}

#[tokio::test]
async fn test_list()
{
    let list_scores = dndrs::APIClient::list::<AbilityScore>().await;
    assert!(list_scores.len() > 0);
    for item in list_scores
    {
        assert!(item.index.len() > 0)
    }
}

#[tokio::test]
async fn test_all()
{
    let all_scores = dndrs::APIClient::all::<AbilityScore>().await;
    assert!(all_scores.len() > 0);
    for score in all_scores
    {
        assert!(score.index.len() > 0);
    }
}
