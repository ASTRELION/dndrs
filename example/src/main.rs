use dndrs::monsters::Monster;

#[tokio::main]
async fn main()
{
    println!("DnD.rs Example Project");

    // Get a specific item by index (note the `Monster` type, an annotation is required)
    let aboleth = dndrs::APIClient::get::<Monster>("aboleth".into()).await;
    println!("{:?}", aboleth);

    // Enumerate the items available at an endpoint (all the monsters)
    // Note this returns `Vec<ListItem>`, *not* `Vec<Monster>`
    let monster_list = dndrs::APIClient::list::<Monster>().await;
    println!("{:?}", monster_list);

    // Get all items of an endpoint
    // Note this returns `Vec<Monster>` (as opposed to above example)
    let all_monsters = dndrs::APIClient::all::<Monster>().await;
    println!("{:?}", all_monsters);
}
