# DnD.rs

[![pipeline status](https://gitlab.com/ASTRELION/dndrs/badges/main/pipeline.svg)](https://gitlab.com/ASTRELION/dndrs/-/commits/main)

[D&D 5e API](https://www.dnd5eapi.co/) client written for Rust.

## Installing

Add the following to your `Cargo.toml`:

```toml
[dependencies]
dndrs = { git = "https://gitlab.com/ASTRELION/dndrs.git", branch = "main" }
```

## Usage

See the [example project](./example/).

## Documentation

https://astrelion.gitlab.io/dndrs/dndrs/
