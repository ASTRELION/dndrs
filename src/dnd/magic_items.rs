use serde::{Deserialize, Serialize};

pub type MagicItems = Vec<MagicItem>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MagicItem
{
    pub index: String,
    pub name: String,
    pub equipment_category: EquipmentCategory,
    pub rarity: Rarity,
    pub variants: Vec<EquipmentCategory>,
    pub variant: bool,
    pub desc: Vec<String>,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct EquipmentCategory
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Rarity
{
    pub name: Name,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Name
{
    Artifact,
    Common,
    Legendary,
    Rare,
    Uncommon,
    Varies,
    #[serde(rename = "Very Rare")]
    VeryRare,
}
