use serde::{Deserialize, Serialize};

pub type Skills = Vec<Skill>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Skill
{
    pub index: String,
    pub name: String,
    pub desc: Vec<String>,
    pub ability_score: AbilityScore,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AbilityScore
{
    pub index: String,
    pub name: String,
    pub url: String,
}
