use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub type Spells = Vec<Spell>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Spell
{
    pub index: String,
    pub name: String,
    pub desc: Vec<String>,
    pub higher_level: Option<Vec<String>>,
    pub range: Range,
    pub components: Vec<Component>,
    pub material: Option<String>,
    pub ritual: bool,
    pub duration: String,
    pub concentration: bool,
    pub casting_time: CastingTime,
    pub level: i64,
    pub attack_type: Option<AttackType>,
    pub damage: Option<Damage>,
    pub school: School,
    pub classes: Vec<School>,
    pub subclasses: Vec<School>,
    pub url: String,
    pub dc: Option<Dc>,
    pub heal_at_slot_level: Option<HashMap<String, String>>,
    pub area_of_effect: Option<AreaOfEffect>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AreaOfEffect
{
    #[serde(rename = "type")]
    pub area_of_effect_type: Type,
    pub size: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Type
{
    Cone,
    Cube,
    Cylinder,
    Line,
    Sphere,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum AttackType
{
    Melee,
    Ranged,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum CastingTime
{
    #[serde(rename = "10 minutes")]
    The10Minutes,
    #[serde(rename = "12 hours")]
    The12Hours,
    #[serde(rename = "1 action")]
    The1Action,
    #[serde(rename = "1 bonus action")]
    The1BonusAction,
    #[serde(rename = "1 hour")]
    The1Hour,
    #[serde(rename = "1 minute")]
    The1Minute,
    #[serde(rename = "1 reaction")]
    The1Reaction,
    #[serde(rename = "24 hours")]
    The24Hours,
    #[serde(rename = "8 hours")]
    The8Hours,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct School
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Component
{
    M,
    S,
    V,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Damage
{
    pub damage_type: Option<School>,
    pub damage_at_slot_level: Option<HashMap<String, String>>,
    pub damage_at_character_level: Option<HashMap<String, String>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Dc
{
    pub dc_type: School,
    pub dc_success: DcSuccess,
    pub desc: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum DcSuccess
{
    Half,
    None,
    Other,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Range
{
    #[serde(rename = "Self")]
    RangeSelf,
    Sight,
    Special,
    #[serde(rename = "100 feet")]
    The100Feet,
    #[serde(rename = "10 feet")]
    The10Feet,
    #[serde(rename = "120 feet")]
    The120Feet,
    #[serde(rename = "150 feet")]
    The150Feet,
    #[serde(rename = "1 mile")]
    The1Mile,
    #[serde(rename = "300 feet")]
    The300Feet,
    #[serde(rename = "30 feet")]
    The30Feet,
    #[serde(rename = "500 feet")]
    The500Feet,
    #[serde(rename = "500 miles")]
    The500Miles,
    #[serde(rename = "5 feet")]
    The5Feet,
    #[serde(rename = "60 feet")]
    The60Feet,
    #[serde(rename = "90 feet")]
    The90Feet,
    Touch,
    Unlimited,
}
