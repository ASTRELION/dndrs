use serde::{Deserialize, Serialize};

pub type Backgrounds = Vec<Background>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Background
{
    pub index: String,
    pub name: String,
    pub starting_proficiencies: Vec<StartingProficiency>,
    pub language_options: LanguageOptions,
    pub starting_equipment: Vec<StartingEquipment>,
    pub starting_equipment_options: Vec<StartingEquipmentOption>,
    pub feature: Feature,
    pub personality_traits: Bonds,
    pub ideals: Ideals,
    pub bonds: Bonds,
    pub flaws: Bonds,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Bonds
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub bonds_type: String,
    pub from: BondsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct BondsFrom
{
    pub option_set_type: String,
    pub options: Vec<PurpleOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PurpleOption
{
    pub option_type: OptionType,
    pub string: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionType
{
    String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Feature
{
    pub name: String,
    pub desc: Vec<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Ideals
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub ideals_type: String,
    pub from: IdealsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct IdealsFrom
{
    pub option_set_type: String,
    pub options: Vec<FluffyOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct FluffyOption
{
    pub option_type: String,
    pub desc: String,
    pub alignments: Vec<StartingProficiency>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StartingProficiency
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct LanguageOptions
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub language_options_type: String,
    pub from: LanguageOptionsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct LanguageOptionsFrom
{
    pub option_set_type: String,
    pub resource_list_url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StartingEquipment
{
    pub equipment: StartingProficiency,
    pub quantity: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StartingEquipmentOption
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub starting_equipment_option_type: String,
    pub from: StartingEquipmentOptionFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StartingEquipmentOptionFrom
{
    pub option_set_type: String,
    pub equipment_category: StartingProficiency,
}
