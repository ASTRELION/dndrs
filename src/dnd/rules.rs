use serde::{Deserialize, Serialize};

pub type Rules = Vec<Rule>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Rule
{
    pub name: String,
    pub index: String,
    pub desc: String,
    pub subsections: Vec<Subsection>,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Subsection
{
    pub name: String,
    pub index: String,
    pub url: String,
}
