use serde::{Deserialize, Serialize};

pub type Races = Vec<Race>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Race
{
    pub index: String,
    pub name: String,
    pub speed: i64,
    pub ability_bonuses: Vec<AbilityBonus>,
    pub alignment: String,
    pub age: String,
    pub size: String,
    pub size_description: String,
    pub starting_proficiencies: Vec<Language>,
    pub starting_proficiency_options: Option<Options>,
    pub languages: Vec<Language>,
    pub language_desc: String,
    pub traits: Vec<Language>,
    pub subraces: Vec<Language>,
    pub url: String,
    pub language_options: Option<Options>,
    pub ability_bonus_options: Option<AbilityBonusOptions>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AbilityBonusOptions
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub ability_bonus_options_type: String,
    pub from: AbilityBonusOptionsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AbilityBonusOptionsFrom
{
    pub option_set_type: String,
    pub options: Vec<PurpleOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PurpleOption
{
    pub option_type: String,
    pub ability_score: Language,
    pub bonus: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Language
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AbilityBonus
{
    pub ability_score: Language,
    pub bonus: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Options
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub options_type: String,
    pub from: LanguageOptionsFrom,
    pub desc: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct LanguageOptionsFrom
{
    pub option_set_type: String,
    pub options: Vec<FluffyOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct FluffyOption
{
    pub option_type: OptionType,
    pub item: Language,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionType
{
    Reference,
}
