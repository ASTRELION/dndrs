use serde::{Deserialize, Serialize};

pub type WeaponProperties = Vec<WeaponProperty>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct WeaponProperty
{
    pub index: String,
    pub name: String,
    pub desc: Vec<String>,
    pub url: String,
}
