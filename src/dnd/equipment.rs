use serde::{Deserialize, Serialize};

pub type Equipments = Vec<Equipment>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Equipment
{
    pub index: String,
    pub name: String,
    pub equipment_category: EquipmentCategory,
    pub weapon_category: Option<WeaponCategory>,
    pub weapon_range: Option<WeaponRange>,
    pub category_range: Option<CategoryRange>,
    pub cost: Cost,
    pub damage: Option<Damage>,
    pub range: Option<Range>,
    pub weight: Option<f64>,
    pub properties: Option<Vec<EquipmentCategory>>,
    pub url: String,
    pub throw_range: Option<Range>,
    pub two_handed_damage: Option<Damage>,
    pub special: Option<Vec<String>>,
    pub armor_category: Option<String>,
    pub armor_class: Option<ArmorClass>,
    pub str_minimum: Option<i64>,
    pub stealth_disadvantage: Option<bool>,
    pub gear_category: Option<EquipmentCategory>,
    pub desc: Option<Vec<String>>,
    pub quantity: Option<i64>,
    pub contents: Option<Vec<Content>>,
    pub tool_category: Option<ToolCategory>,
    pub vehicle_category: Option<VehicleCategory>,
    pub speed: Option<Cost>,
    pub capacity: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ArmorClass
{
    pub base: i64,
    pub dex_bonus: bool,
    pub max_bonus: Option<i64>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum CategoryRange
{
    #[serde(rename = "Martial Melee")]
    MartialMelee,
    #[serde(rename = "Martial Ranged")]
    MartialRanged,
    #[serde(rename = "Simple Melee")]
    SimpleMelee,
    #[serde(rename = "Simple Ranged")]
    SimpleRanged,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Content
{
    pub item: EquipmentCategory,
    pub quantity: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct EquipmentCategory
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Cost
{
    pub quantity: f64,
    pub unit: Unit,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Unit
{
    Cp,
    #[serde(rename = "ft/round")]
    FtRound,
    Gp,
    Mph,
    Sp,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Damage
{
    pub damage_dice: String,
    pub damage_type: EquipmentCategory,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Range
{
    pub normal: i64,
    pub long: Option<i64>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum ToolCategory
{
    #[serde(rename = "Artisan's Tools")]
    ArtisanSTools,
    #[serde(rename = "Gaming Sets")]
    GamingSets,
    #[serde(rename = "Musical Instrument")]
    MusicalInstrument,
    #[serde(rename = "Other Tools")]
    OtherTools,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum VehicleCategory
{
    #[serde(rename = "Mounts and Other Animals")]
    MountsAndOtherAnimals,
    #[serde(rename = "Tack, Harness, and Drawn Vehicles")]
    TackHarnessAndDrawnVehicles,
    #[serde(rename = "Waterborne Vehicles")]
    WaterborneVehicles,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum WeaponCategory
{
    Martial,
    Simple,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum WeaponRange
{
    Melee,
    Ranged,
}
