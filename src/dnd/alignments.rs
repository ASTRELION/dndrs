use serde::{Deserialize, Serialize};

pub type Alignments = Vec<Alignment>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Alignment
{
    pub index: String,
    pub name: String,
    pub abbreviation: String,
    pub desc: String,
    pub url: String,
}
