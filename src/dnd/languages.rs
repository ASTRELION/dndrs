use serde::{Deserialize, Serialize};

pub type Languages = Vec<Language>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Language
{
    pub index: String,
    pub name: String,
    #[serde(rename = "type")]
    pub language_type: Type,
    pub typical_speakers: Vec<String>,
    pub script: Option<String>,
    pub url: String,
    pub desc: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Type
{
    Exotic,
    Standard,
}
