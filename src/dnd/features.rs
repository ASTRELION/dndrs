use serde::{Deserialize, Serialize};

pub type Features = Vec<Feature>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Feature
{
    pub index: String,
    pub class: Class,
    pub name: String,
    pub level: i64,
    pub prerequisites: Vec<Prerequisite>,
    pub desc: Vec<String>,
    pub url: String,
    pub subclass: Option<Class>,
    pub reference: Option<String>,
    pub feature_specific: Option<FeatureSpecific>,
    pub parent: Option<Class>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Class
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct FeatureSpecific
{
    pub expertise_options: Option<ExpertiseOptions>,
    pub subfeature_options: Option<SubfeatureOptions>,
    pub invocations: Option<Vec<Class>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ExpertiseOptions
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub expertise_options_type: SubfeatureOptionsType,
    pub from: ExpertiseOptionsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum SubfeatureOptionsType
{
    Feature,
    Proficiency,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ExpertiseOptionsFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<ItemElement>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionSetType
{
    #[serde(rename = "options_array")]
    OptionsArray,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ItemElement
{
    pub option_type: OptionType,
    pub item: Option<Class>,
    pub choice: Option<SubfeatureOptions>,
    pub items: Option<Vec<ItemElement>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SubfeatureOptions
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub subfeature_options_type: SubfeatureOptionsType,
    pub from: SubfeatureOptionsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SubfeatureOptionsFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<PurpleOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PurpleOption
{
    pub option_type: OptionType,
    pub item: Class,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionType
{
    Choice,
    Multiple,
    Reference,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Prerequisite
{
    #[serde(rename = "type")]
    pub prerequisite_type: PrerequisiteType,
    pub spell: Option<String>,
    pub feature: Option<String>,
    pub level: Option<i64>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum PrerequisiteType
{
    Feature,
    Level,
    Spell,
}
