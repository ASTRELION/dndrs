use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub type Traits = Vec<Trait>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Trait
{
    pub index: String,
    pub races: Vec<Parent>,
    pub subraces: Vec<Parent>,
    pub name: String,
    pub desc: Vec<String>,
    pub proficiencies: Vec<Parent>,
    pub url: String,
    pub proficiency_choices: Option<LanguageOptions>,
    pub trait_specific: Option<TraitSpecific>,
    pub language_options: Option<LanguageOptions>,
    pub parent: Option<Parent>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct LanguageOptions
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub language_options_type: String,
    pub from: From,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct From
{
    pub option_set_type: String,
    pub options: Vec<Options>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Options
{
    pub option_type: OptionType,
    pub item: Parent,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Parent
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionType
{
    Reference,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct TraitSpecific
{
    pub spell_options: Option<LanguageOptions>,
    pub subtrait_options: Option<LanguageOptions>,
    pub damage_type: Option<Parent>,
    pub breath_weapon: Option<BreathWeapon>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct BreathWeapon
{
    pub name: Name,
    pub desc: String,
    pub area_of_effect: AreaOfEffect,
    pub usage: Usage,
    pub dc: Dc,
    pub damage: Vec<Damage>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AreaOfEffect
{
    pub size: i64,
    #[serde(rename = "type")]
    pub area_of_effect_type: AreaOfEffectType,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum AreaOfEffectType
{
    Cone,
    Line,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Damage
{
    pub damage_type: Parent,
    pub damage_at_character_level: HashMap<String, DamageAtCharacterLevel>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum DamageAtCharacterLevel
{
    #[serde(rename = "2d6")]
    The2D6,
    #[serde(rename = "3d6")]
    The3D6,
    #[serde(rename = "4d6")]
    The4D6,
    #[serde(rename = "5d6")]
    The5D6,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Dc
{
    pub dc_type: Parent,
    pub success_type: SuccessType,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum SuccessType
{
    Half,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Name
{
    #[serde(rename = "Breath Weapon")]
    BreathWeapon,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Usage
{
    #[serde(rename = "type")]
    pub usage_type: UsageType,
    pub times: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum UsageType
{
    #[serde(rename = "per rest")]
    PerRest,
}
