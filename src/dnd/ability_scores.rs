use serde::{Deserialize, Serialize};

pub type AbilityScores = Vec<AbilityScore>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AbilityScore
{
    pub index: String,
    pub name: String,
    pub full_name: String,
    pub desc: Vec<String>,
    pub skills: Vec<Skill>,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Skill
{
    pub name: String,
    pub index: String,
    pub url: String,
}
