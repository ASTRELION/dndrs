use serde::{Deserialize, Serialize};

pub type Classes = Vec<Class>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Class
{
    pub index: String,
    pub name: String,
    pub hit_die: i64,
    pub proficiency_choices: Vec<ProficiencyChoice>,
    pub proficiencies: Vec<Proficiency>,
    pub saving_throws: Vec<Proficiency>,
    pub starting_equipment: Vec<StartingEquipment>,
    pub starting_equipment_options: Vec<StartingEquipmentOption>,
    pub class_levels: String,
    pub multi_classing: MultiClassing,
    pub subclasses: Vec<Proficiency>,
    pub url: String,
    pub spellcasting: Option<Spellcasting>,
    pub spells: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MultiClassing
{
    pub prerequisites: Option<Vec<MultiClassingPrerequisite>>,
    pub proficiencies: Vec<Proficiency>,
    pub proficiency_choices: Option<Vec<ProficiencyChoiceElement>>,
    pub prerequisite_options: Option<PrerequisiteOptions>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PrerequisiteOptions
{
    #[serde(rename = "type")]
    pub prerequisite_options_type: String,
    pub choose: i64,
    pub from: PrerequisiteOptionsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PrerequisiteOptionsFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<PurpleOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionSetType
{
    #[serde(rename = "equipment_category")]
    EquipmentCategory,
    #[serde(rename = "options_array")]
    OptionsArray,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PurpleOption
{
    pub option_type: String,
    pub ability_score: Proficiency,
    pub minimum_score: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Proficiency
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MultiClassingPrerequisite
{
    pub ability_score: Proficiency,
    pub minimum_score: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ProficiencyChoiceElement
{
    pub desc: Option<String>,
    pub choose: i64,
    #[serde(rename = "type")]
    pub choice_type: ProficiencyChoiceType,
    pub from: PurpleFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ProficiencyChoiceType
{
    Proficiencies,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PurpleFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<FluffyOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct FluffyOption
{
    pub option_type: PurpleOptionType,
    pub item: Proficiency,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum PurpleOptionType
{
    Choice,
    Reference,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ProficiencyChoice
{
    pub desc: String,
    pub choose: i64,
    #[serde(rename = "type")]
    pub proficiency_choice_type: ProficiencyChoiceType,
    pub from: FluffyFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct FluffyFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<TentacledOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct TentacledOption
{
    pub option_type: PurpleOptionType,
    pub item: Option<Proficiency>,
    pub choice: Option<ProficiencyChoiceElement>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Spellcasting
{
    pub level: i64,
    pub spellcasting_ability: Proficiency,
    pub info: Vec<Info>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Info
{
    pub name: String,
    pub desc: Vec<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StartingEquipment
{
    pub equipment: Proficiency,
    pub quantity: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StartingEquipmentOption
{
    pub desc: String,
    pub choose: i64,
    #[serde(rename = "type")]
    pub starting_equipment_option_type: StartingEquipmentOptionType,
    pub from: StartingEquipmentOptionFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StartingEquipmentOptionFrom
{
    pub option_set_type: OptionSetType,
    pub options: Option<Vec<StickyOption>>,
    pub equipment_category: Option<Proficiency>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct StickyOption
{
    pub option_type: ItemOptionType,
    pub count: Option<i64>,
    pub of: Option<Proficiency>,
    pub choice: Option<ItemChoice>,
    pub prerequisites: Option<Vec<OptionPrerequisite>>,
    pub items: Option<Vec<Item>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ItemChoice
{
    pub desc: String,
    pub choose: i64,
    #[serde(rename = "type")]
    pub choice_type: StartingEquipmentOptionType,
    pub from: TentacledFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum StartingEquipmentOptionType
{
    Equipment,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct TentacledFrom
{
    pub option_set_type: OptionSetType,
    pub equipment_category: Proficiency,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Item
{
    pub option_type: ItemOptionType,
    pub count: Option<i64>,
    pub of: Option<Proficiency>,
    pub choice: Option<ItemChoice>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ItemOptionType
{
    Choice,
    #[serde(rename = "counted_reference")]
    CountedReference,
    Multiple,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct OptionPrerequisite
{
    #[serde(rename = "type")]
    pub prerequisite_type: String,
    pub proficiency: Proficiency,
}
