use serde::{Deserialize, Serialize};

pub type Conditions = Vec<Condition>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Condition
{
    pub index: String,
    pub name: String,
    pub desc: Vec<String>,
    pub url: String,
}
