use serde::{Deserialize, Serialize};

pub type Subclasses = Vec<Subclass>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Subclass
{
    pub index: String,
    pub class: Class,
    pub name: String,
    pub subclass_flavor: String,
    pub desc: Vec<String>,
    pub subclass_levels: String,
    pub url: String,
    pub spells: Option<Vec<Spell>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Class
{
    pub index: String,
    pub name: String,
    pub url: String,
    #[serde(rename = "type")]
    pub class_type: Option<Type>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Type
{
    Feature,
    Level,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Spell
{
    pub prerequisites: Vec<Class>,
    pub spell: Class,
}
