use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub type Monsters = Vec<Monster>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Monster
{
    pub index: String,
    pub name: String,
    pub size: Size,
    #[serde(rename = "type")]
    pub monster_type: MonsterType,
    pub alignment: Alignment,
    pub armor_class: Vec<ArmorClass>,
    pub hit_points: i64,
    pub hit_dice: String,
    pub hit_points_roll: String,
    pub speed: Speed,
    pub strength: i64,
    pub dexterity: i64,
    pub constitution: i64,
    pub intelligence: i64,
    pub wisdom: i64,
    pub charisma: i64,
    pub proficiencies: Vec<Proficiency>,
    pub damage_vulnerabilities: Vec<String>,
    pub damage_resistances: Vec<String>,
    pub damage_immunities: Vec<String>,
    pub condition_immunities: Vec<ConditionImmunity>,
    pub senses: Senses,
    pub languages: String,
    pub challenge_rating: f64,
    pub proficiency_bonus: i64,
    pub xp: i64,
    pub special_abilities: Option<Vec<SpecialAbility>>,
    pub actions: Option<Vec<MonsterAction>>,
    pub legendary_actions: Option<Vec<LegendaryAction>>,
    pub image: Option<String>,
    pub url: String,
    pub desc: Option<String>,
    pub subtype: Option<String>,
    pub reactions: Option<Vec<Reaction>>,
    pub forms: Option<Vec<ConditionImmunity>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MonsterAction
{
    pub name: String,
    pub multiattack_type: Option<MultiattackType>,
    pub desc: String,
    pub actions: Option<Vec<ActionAction>>,
    pub attack_bonus: Option<i64>,
    pub dc: Option<Dc>,
    pub damage: Option<Vec<ActionDamage>>,
    pub usage: Option<ActionUsage>,
    pub options: Option<Options>,
    pub attacks: Option<Vec<Attack>>,
    pub action_options: Option<ActionOptions>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ActionOptions
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub action_options_type: ItemOptionType,
    pub from: ActionOptionsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ItemOptionType
{
    Action,
    Multiple,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ActionOptionsFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<ItemElement>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionSetType
{
    #[serde(rename = "options_array")]
    OptionsArray,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ItemElement
{
    pub option_type: ItemOptionType,
    pub items: Option<Vec<ItemElement>>,
    pub action_name: Option<String>,
    pub count: Option<i64>,
    #[serde(rename = "type")]
    pub purple_type: Option<ActionType>,
    pub desc: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ActionType
{
    Ability,
    Magic,
    Melee,
    Ranged,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ActionAction
{
    pub action_name: String,
    pub count: Count,
    #[serde(rename = "type")]
    pub action_type: ActionType,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Count
{
    Integer(i64),
    String(String),
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Attack
{
    pub name: String,
    pub dc: Dc,
    pub damage: Option<Vec<AttackDamage>>,
    pub option_type: Option<OptionType>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AttackDamage
{
    pub damage_type: ConditionImmunity,
    pub damage_dice: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ConditionImmunity
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Dc
{
    pub dc_type: ConditionImmunity,
    pub dc_value: i64,
    pub success_type: SuccessType,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum SuccessType
{
    Half,
    None,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionType
{
    Breath,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ActionDamage
{
    pub damage_type: Option<ConditionImmunity>,
    pub damage_dice: Option<String>,
    pub dc: Option<Dc>,
    pub choose: Option<i64>,
    #[serde(rename = "type")]
    pub purple_type: Option<DamageType>,
    pub from: Option<DamageFrom>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct DamageFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<PurpleOption>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct PurpleOption
{
    pub option_type: DamageType,
    pub damage_type: ConditionImmunity,
    pub damage_dice: String,
    pub notes: Option<Notes>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Notes
{
    #[serde(rename = "One handed")]
    OneHanded,
    #[serde(rename = "Two handed")]
    TwoHanded,
    #[serde(rename = "With shillelagh")]
    WithShillelagh,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum DamageType
{
    Damage,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum MultiattackType
{
    #[serde(rename = "action_options")]
    ActionOptions,
    Actions,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Options
{
    pub choose: i64,
    #[serde(rename = "type")]
    pub options_type: OptionsType,
    pub from: OptionsFrom,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct OptionsFrom
{
    pub option_set_type: OptionSetType,
    pub options: Vec<Attack>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionsType
{
    Attack,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ActionUsage
{
    #[serde(rename = "type")]
    pub usage_type: PurpleType,
    pub times: Option<i64>,
    pub dice: Option<Dice>,
    pub min_value: Option<i64>,
    pub rest_types: Option<Vec<RestType>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Dice
{
    #[serde(rename = "1d6")]
    The1D6,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum RestType
{
    Long,
    Short,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum PurpleType
{
    #[serde(rename = "per day")]
    PerDay,
    #[serde(rename = "recharge after rest")]
    RechargeAfterRest,
    #[serde(rename = "recharge on roll")]
    RechargeOnRoll,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Alignment
{
    #[serde(rename = "any alignment")]
    AnyAlignment,
    #[serde(rename = "any chaotic alignment")]
    AnyChaoticAlignment,
    #[serde(rename = "any evil alignment")]
    AnyEvilAlignment,
    #[serde(rename = "any non-good alignment")]
    AnyNonGoodAlignment,
    #[serde(rename = "any non-lawful alignment")]
    AnyNonLawfulAlignment,
    #[serde(rename = "chaotic evil")]
    ChaoticEvil,
    #[serde(rename = "chaotic good")]
    ChaoticGood,
    #[serde(rename = "chaotic neutral")]
    ChaoticNeutral,
    #[serde(rename = "lawful evil")]
    LawfulEvil,
    #[serde(rename = "lawful good")]
    LawfulGood,
    #[serde(rename = "lawful neutral")]
    LawfulNeutral,
    Neutral,
    #[serde(rename = "neutral evil")]
    NeutralEvil,
    #[serde(rename = "neutral good")]
    NeutralGood,
    #[serde(rename = "neutral good (50%) or neutral evil (50%)")]
    NeutralGood50OrNeutralEvil50,
    Unaligned,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ArmorClass
{
    #[serde(rename = "type")]
    pub armor_class_type: ArmorClassType,
    pub value: i64,
    pub condition: Option<ConditionImmunity>,
    pub spell: Option<ConditionImmunity>,
    pub armor: Option<Vec<ConditionImmunity>>,
    pub desc: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ArmorClassType
{
    Armor,
    Condition,
    Dex,
    Natural,
    Spell,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct LegendaryAction
{
    pub name: String,
    pub desc: String,
    pub attack_bonus: Option<i64>,
    pub damage: Option<Vec<AttackDamage>>,
    pub dc: Option<Dc>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum MonsterType
{
    Aberration,
    Beast,
    Celestial,
    Construct,
    Dragon,
    Elemental,
    Fey,
    Fiend,
    Giant,
    Humanoid,
    Monstrosity,
    Ooze,
    Plant,
    #[serde(rename = "swarm of Tiny beasts")]
    SwarmOfTinyBeasts,
    Undead,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Proficiency
{
    pub value: i64,
    pub proficiency: ConditionImmunity,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Reaction
{
    pub name: String,
    pub desc: String,
    pub dc: Option<Dc>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Senses
{
    pub darkvision: Option<Blindsight>,
    pub passive_perception: i64,
    pub blindsight: Option<Blindsight>,
    pub truesight: Option<Blindsight>,
    pub tremorsense: Option<Blindsight>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Blindsight
{
    #[serde(rename = "10 ft.")]
    The10Ft,
    #[serde(rename = "120 ft.")]
    The120Ft,
    #[serde(rename = "30 ft.")]
    The30Ft,
    #[serde(rename = "30 ft. (blind beyond this radius)")]
    The30FtBlindBeyondThisRadius,
    #[serde(rename = "30 ft. or 10 ft. while deafened (blind beyond this radius)")]
    The30FtOr10FtWhileDeafenedBlindBeyondThisRadius,
    #[serde(rename = "60 ft.")]
    The60Ft,
    #[serde(rename = "60 ft. (blind beyond this radius)")]
    The60FtBlindBeyondThisRadius,
    #[serde(rename = "90 ft.")]
    The90Ft,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Size
{
    Gargantuan,
    Huge,
    Large,
    Medium,
    Small,
    Tiny,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SpecialAbility
{
    pub name: String,
    pub desc: String,
    pub dc: Option<Dc>,
    pub spellcasting: Option<Spellcasting>,
    pub usage: Option<SpecialAbilityUsage>,
    pub damage: Option<Vec<AttackDamage>>,
    pub attack_bonus: Option<i64>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Spellcasting
{
    pub level: Option<i64>,
    pub ability: ConditionImmunity,
    pub dc: Option<i64>,
    pub modifier: Option<i64>,
    pub components_required: Vec<ComponentsRequired>,
    pub school: Option<School>,
    pub slots: Option<HashMap<String, i64>>,
    pub spells: Vec<Spell>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum ComponentsRequired
{
    M,
    S,
    V,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum School
{
    Cleric,
    Druid,
    Wizard,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Spell
{
    pub name: String,
    pub level: i64,
    pub url: String,
    pub usage: Option<SpellUsage>,
    pub notes: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SpellUsage
{
    #[serde(rename = "type")]
    pub usage_type: FluffyType,
    pub times: Option<i64>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum FluffyType
{
    #[serde(rename = "at will")]
    AtWill,
    #[serde(rename = "per day")]
    PerDay,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SpecialAbilityUsage
{
    #[serde(rename = "type")]
    pub usage_type: PurpleType,
    pub times: Option<i64>,
    pub rest_types: Option<Vec<RestType>>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Speed
{
    pub walk: Option<Burrow>,
    pub swim: Option<Climb>,
    pub fly: Option<Climb>,
    pub burrow: Option<Burrow>,
    pub climb: Option<Climb>,
    pub hover: Option<bool>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Burrow
{
    #[serde(rename = "0 ft.")]
    The0Ft,
    #[serde(rename = "10 ft.")]
    The10Ft,
    #[serde(rename = "15 ft.")]
    The15Ft,
    #[serde(rename = "20 ft.")]
    The20Ft,
    #[serde(rename = "25 ft.")]
    The25Ft,
    #[serde(rename = "30 ft.")]
    The30Ft,
    #[serde(rename = "40 ft.")]
    The40Ft,
    #[serde(rename = "50 ft.")]
    The50Ft,
    #[serde(rename = "5 ft.")]
    The5Ft,
    #[serde(rename = "60 ft.")]
    The60Ft,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Climb
{
    #[serde(rename = "10 ft.")]
    The10Ft,
    #[serde(rename = "120 ft.")]
    The120Ft,
    #[serde(rename = "150 ft.")]
    The150Ft,
    #[serde(rename = "20 ft.")]
    The20Ft,
    #[serde(rename = "30 ft.")]
    The30Ft,
    #[serde(rename = "40 ft.")]
    The40Ft,
    #[serde(rename = "50 ft.")]
    The50Ft,
    #[serde(rename = "60 ft.")]
    The60Ft,
    #[serde(rename = "80 ft.")]
    The80Ft,
    #[serde(rename = "90 ft.")]
    The90Ft,
}
