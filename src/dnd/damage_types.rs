use serde::{Deserialize, Serialize};

pub type DamageTypes = Vec<DamageType>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct DamageType
{
    pub index: String,
    pub name: String,
    pub desc: Vec<String>,
    pub url: String,
}
