use serde::{Deserialize, Serialize};

pub type Feats = Vec<Feat>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Feat
{
    pub index: String,
    pub name: String,
    pub prerequisites: Vec<Prerequisite>,
    pub desc: Vec<String>,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Prerequisite
{
    pub ability_score: AbilityScore,
    pub minimum_score: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AbilityScore
{
    pub index: String,
    pub name: String,
    pub url: String,
}
