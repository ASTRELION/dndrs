use serde::{Deserialize, Serialize};

pub type Subraces = Vec<Subrace>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Subrace
{
    pub index: String,
    pub name: String,
    pub race: Race,
    pub desc: String,
    pub ability_bonuses: Vec<AbilityBonus>,
    pub starting_proficiencies: Vec<Race>,
    pub languages: Vec<Option<serde_json::Value>>,
    pub racial_traits: Vec<Race>,
    pub url: String,
    pub language_options: Option<LanguageOptions>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct AbilityBonus
{
    pub ability_score: Race,
    pub bonus: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Race
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct LanguageOptions
{
    pub choose: i64,
    pub from: From,
    #[serde(rename = "type")]
    pub language_options_type: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct From
{
    pub option_set_type: String,
    pub options: Vec<Options>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Options
{
    pub option_type: OptionType,
    pub item: Race,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum OptionType
{
    Reference,
}
