use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub type Levels = Vec<Level>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Level
{
    pub level: i64,
    pub ability_score_bonuses: Option<i64>,
    pub prof_bonus: Option<i64>,
    pub features: Vec<Class>,
    pub class_specific: Option<ClassSpecific>,
    pub index: String,
    pub class: Class,
    pub url: String,
    pub spellcasting: Option<HashMap<String, i64>>,
    pub subclass: Option<Class>,
    pub subclass_specific: Option<SubclassSpecific>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Class
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ClassSpecific
{
    pub rage_count: Option<i64>,
    pub rage_damage_bonus: Option<i64>,
    pub brutal_critical_dice: Option<i64>,
    pub bardic_inspiration_die: Option<i64>,
    pub song_of_rest_die: Option<i64>,
    pub magical_secrets_max_5: Option<i64>,
    pub magical_secrets_max_7: Option<i64>,
    pub magical_secrets_max_9: Option<i64>,
    pub channel_divinity_charges: Option<i64>,
    pub destroy_undead_cr: Option<f64>,
    pub wild_shape_max_cr: Option<f64>,
    pub wild_shape_swim: Option<bool>,
    pub wild_shape_fly: Option<bool>,
    pub action_surges: Option<i64>,
    pub indomitable_uses: Option<i64>,
    pub extra_attacks: Option<i64>,
    pub martial_arts: Option<MartialArts>,
    pub ki_points: Option<i64>,
    pub unarmored_movement: Option<i64>,
    pub aura_range: Option<i64>,
    pub favored_enemies: Option<i64>,
    pub favored_terrain: Option<i64>,
    pub sneak_attack: Option<MartialArts>,
    pub sorcery_points: Option<i64>,
    pub metamagic_known: Option<i64>,
    pub creating_spell_slots: Option<Vec<CreatingSpellSlot>>,
    pub invocations_known: Option<i64>,
    pub mystic_arcanum_level_6: Option<i64>,
    pub mystic_arcanum_level_7: Option<i64>,
    pub mystic_arcanum_level_8: Option<i64>,
    pub mystic_arcanum_level_9: Option<i64>,
    pub arcane_recovery_levels: Option<i64>,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct CreatingSpellSlot
{
    pub spell_slot_level: i64,
    pub sorcery_point_cost: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MartialArts
{
    pub dice_count: i64,
    pub dice_value: i64,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct SubclassSpecific
{
    pub additional_magical_secrets_max_lvl: Option<i64>,
    pub aura_range: Option<i64>,
}
