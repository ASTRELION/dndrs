use serde::{Deserialize, Serialize};

pub type Proficiencies = Vec<Proficiency>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Proficiency
{
    pub index: String,
    #[serde(rename = "type")]
    pub proficiency_type: Type,
    pub name: String,
    pub classes: Vec<Reference>,
    pub races: Vec<Reference>,
    pub url: String,
    pub reference: Reference,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Reference
{
    pub index: String,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum Type
{
    Armor,
    #[serde(rename = "Artisan's Tools")]
    ArtisanSTools,
    #[serde(rename = "Gaming Sets")]
    GamingSets,
    #[serde(rename = "Musical Instruments")]
    MusicalInstruments,
    Other,
    #[serde(rename = "Saving Throws")]
    SavingThrows,
    Skills,
    Vehicles,
    Weapons,
}
