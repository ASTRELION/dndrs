use serde::{Deserialize, Serialize};

pub type MagicSchools = Vec<MagicSchool>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct MagicSchool
{
    pub index: String,
    pub name: String,
    pub desc: String,
    pub url: String,
}
