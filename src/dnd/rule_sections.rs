use serde::{Deserialize, Serialize};

pub type RuleSections = Vec<RuleSection>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct RuleSection
{
    pub name: String,
    pub index: String,
    pub desc: String,
    pub url: String,
}
