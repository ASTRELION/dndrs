use serde::{Deserialize, Serialize};

pub type EquipmentCategories = Vec<EquipmentCategory>;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct EquipmentCategory
{
    pub index: String,
    pub name: String,
    pub equipment: Option<Vec<Equipment>>,
    pub url: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Equipment
{
    pub index: String,
    pub name: String,
    pub url: String,
}
