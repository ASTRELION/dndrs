use futures::future;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::Value;

use crate::dnd::{
    ability_scores::AbilityScore, alignments::Alignment, backgrounds::Background, classes::Class,
    conditions::Condition, damage_types::DamageType, equipment::Equipment,
    equipment_categories::EquipmentCategory, feats::Feat, features::Feature, languages::Language,
    levels::Level, magic_items::MagicItem, magic_schools::MagicSchool, monsters::Monster,
    proficiencies::Proficiency, races::Race, rule_sections::RuleSection, rules::Rule,
    skills::Skill, spells::Spell, subclasses::Subclass, subraces::Subrace, traits::Trait,
    weapon_properties::WeaponProperty,
};

pub static API_URL: &str = "https://www.dnd5eapi.co/api";

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ListItem
{
    pub index: String,
    pub name: String,
    pub url: String,
}

pub struct APIClient;

impl APIClient
{
    pub async fn get<T>(index: String) -> Result<T, Box<dyn std::error::Error + Send + Sync>>
    where
        T: APIPath + DeserializeOwned,
    {
        let url = format!("{}/{}/{}", API_URL, T::api_path(), index);
        let response = reqwest::get(url).await?;
        let body = response.bytes().await.unwrap();
        let item: T = serde_json::from_slice(&body).unwrap();
        return Ok(item);
    }

    pub async fn list<T>() -> Vec<ListItem>
    where
        T: APIPath + DeserializeOwned,
    {
        let url = format!("{}/{}", API_URL, T::api_path());
        let response = reqwest::get(url).await.unwrap();
        let body = response.bytes().await.unwrap();
        let result: Value = serde_json::from_slice(&body).unwrap();
        let value = result.get("results").unwrap().clone();
        let item: Vec<ListItem> = serde_json::from_value(value).unwrap();
        return item;
    }

    pub async fn all<T>() -> Vec<T>
    where
        T: APIPath + DeserializeOwned + Send + 'static,
    {
        let l = APIClient::list::<T>().await;
        let tasks = l
            .into_iter()
            .map(|item| tokio::spawn(APIClient::get::<T>(item.index)));
        let results = future::join_all(tasks).await;
        let items: Vec<T> = results.into_iter().map(|r| r.unwrap().unwrap()).collect();

        return items;
    }
}

pub trait APIPath
{
    fn api_path() -> &'static str;
}

impl APIPath for AbilityScore
{
    fn api_path() -> &'static str
    {
        return "ability-scores";
    }
}

impl APIPath for Alignment
{
    fn api_path() -> &'static str
    {
        return "alignments";
    }
}

impl APIPath for Background
{
    fn api_path() -> &'static str
    {
        return "backgrounds";
    }
}

impl APIPath for Class
{
    fn api_path() -> &'static str
    {
        return "classes";
    }
}

impl APIPath for Condition
{
    fn api_path() -> &'static str
    {
        return "conditions";
    }
}

impl APIPath for DamageType
{
    fn api_path() -> &'static str
    {
        return "damage-types";
    }
}

impl APIPath for EquipmentCategory
{
    fn api_path() -> &'static str
    {
        return "equipment-categories";
    }
}

impl APIPath for Equipment
{
    fn api_path() -> &'static str
    {
        return "equipment";
    }
}

impl APIPath for Feat
{
    fn api_path() -> &'static str
    {
        return "feats";
    }
}

impl APIPath for Feature
{
    fn api_path() -> &'static str
    {
        return "features";
    }
}

impl APIPath for Language
{
    fn api_path() -> &'static str
    {
        return "languages";
    }
}

impl APIPath for Level
{
    fn api_path() -> &'static str
    {
        return "levels";
    }
}

impl APIPath for MagicItem
{
    fn api_path() -> &'static str
    {
        return "magic-items";
    }
}

impl APIPath for MagicSchool
{
    fn api_path() -> &'static str
    {
        return "magic-schools";
    }
}

impl APIPath for Monster
{
    fn api_path() -> &'static str
    {
        return "monsters";
    }
}

impl APIPath for Proficiency
{
    fn api_path() -> &'static str
    {
        return "proficiencies";
    }
}

impl APIPath for Race
{
    fn api_path() -> &'static str
    {
        return "races";
    }
}

impl APIPath for RuleSection
{
    fn api_path() -> &'static str
    {
        return "rule-sections";
    }
}

impl APIPath for Rule
{
    fn api_path() -> &'static str
    {
        return "rules";
    }
}

impl APIPath for Skill
{
    fn api_path() -> &'static str
    {
        return "skills";
    }
}

impl APIPath for Spell
{
    fn api_path() -> &'static str
    {
        return "spells";
    }
}

impl APIPath for Subclass
{
    fn api_path() -> &'static str
    {
        return "subclasses";
    }
}

impl APIPath for Subrace
{
    fn api_path() -> &'static str
    {
        return "subraces";
    }
}

impl APIPath for Trait
{
    fn api_path() -> &'static str
    {
        return "traits";
    }
}

impl APIPath for WeaponProperty
{
    fn api_path() -> &'static str
    {
        return "weapon-properties";
    }
}
