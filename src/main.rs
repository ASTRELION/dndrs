#[allow(dead_code)]
mod api;
#[allow(dead_code)]
mod dnd;

use crate::{api::APIClient, dnd::ability_scores::AbilityScore};

#[tokio::main]
async fn main()
{
    env_logger::init();
    println!("Hello, world!");
    let result = APIClient::all::<AbilityScore>().await;
    println!("{:?}", result);
}
