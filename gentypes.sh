# !/bin/bash

# This script requires quicktype to be available globally.
# https://github.com/glideapps/quicktype?tab=readme-ov-file#installation

base_url="https://raw.githubusercontent.com/5e-bits/5e-database/main/src"
base_dir="src/dnd"

urls=(
    "$base_url/5e-SRD-Ability-Scores.json"
    "$base_url/5e-SRD-Alignments.json"
    "$base_url/5e-SRD-Backgrounds.json"
    "$base_url/5e-SRD-Classes.json"
    "$base_url/5e-SRD-Conditions.json"
    "$base_url/5e-SRD-Damage-Types.json"
    "$base_url/5e-SRD-Equipment-Categories.json"
    "$base_url/5e-SRD-Equipment.json"
    "$base_url/5e-SRD-Feats.json"
    "$base_url/5e-SRD-Features.json"
    "$base_url/5e-SRD-Languages.json"
    "$base_url/5e-SRD-Levels.json"
    "$base_url/5e-SRD-Magic-Items.json"
    "$base_url/5e-SRD-Magic-Schools.json"
    "$base_url/5e-SRD-Monsters.json"
    "$base_url/5e-SRD-Proficiencies.json"
    "$base_url/5e-SRD-Races.json"
    "$base_url/5e-SRD-Rule-Sections.json"
    "$base_url/5e-SRD-Rules.json"
    "$base_url/5e-SRD-Skills.json"
    "$base_url/5e-SRD-Spells.json"
    "$base_url/5e-SRD-Subclasses.json"
    "$base_url/5e-SRD-Subraces.json"
    "$base_url/5e-SRD-Traits.json"
    "$base_url/5e-SRD-Weapon-Properties.json"
)
output_files=(
    "$base_dir/ability_scores.rs"
    "$base_dir/alignments.rs"
    "$base_dir/backgrounds.rs"
    "$base_dir/classes.rs"
    "$base_dir/conditions.rs"
    "$base_dir/damage_types.rs"
    "$base_dir/equipment_categories.rs"
    "$base_dir/equipment.rs"
    "$base_dir/feats.rs"
    "$base_dir/features.rs"
    "$base_dir/languages.rs"
    "$base_dir/levels.rs"
    "$base_dir/magic_items.rs"
    "$base_dir/magic_schools.rs"
    "$base_dir/monsters.rs"
    "$base_dir/proficiencies.rs"
    "$base_dir/races.rs"
    "$base_dir/rule_sections.rs"
    "$base_dir/rules.rs"
    "$base_dir/skills.rs"
    "$base_dir/spells.rs"
    "$base_dir/subclasses.rs"
    "$base_dir/subraces.rs"
    "$base_dir/traits.rs"
    "$base_dir/weapon_properties.rs"
)

for ((i=0; i<${#urls[@]}; i++)); do
    url="${urls[i]}"
    output_file="${output_files[i]}"

    command="quicktype $url -o $output_file --no-leading-comments --visibility public --density dense --derive-debug --derive-clone --derive-partial-eq --telemetry disable --lang rust"
    echo "Executing $command"
    $command
done

cargo fmt
